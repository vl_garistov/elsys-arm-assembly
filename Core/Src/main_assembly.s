/*
 * ELSYS ARM Assembly - Basic examples for teaching ARM assembly
 * Copyright (C) 2024 Vladimir Garistov <vl.garistov@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * main_assembly.s
 *
 *  Created on: Feb 6, 2024
 *      Author: Vladimir Garistov <vl.garistov@gmail.com>
 */

.syntax unified
.cpu cortex-m3
.fpu softvfp
.thumb

.global Main_Assembly
.type Main_Assembly, %function

.section .text.main_assembly
Main_Assembly:
	BL Arr_sort
	BL Blink
	BX LR
.size Main_Assembly, .-Main_Assembly

.section .text.example_functions
//Provides delay in miliseconds indicated by R0
Delay:
	CMP R0, #0
	IT LE
		BXLE LR
	LDR R1, =SYST_CSR
	ADD R0, R0, #1				//Ensure minimum delay
	Delay_Loop:
		LDR R2, [R1]
		ANDS R2, R2, #0x10000	//Sets N=1, Z=0 instead of N=0, Z=1
		CMP R2, #0
		IT NE
			SUBSNE R0, R0, #1
		CMP R0, #0
		BNE Delay_Loop
	BX LR

//Sorts the array
Arr_sort:
	LDR R0, =Arr
	MOV R1, #15				//Array lenght
	MOV R2, #0				//Counter

	Sort_Loop:
		MOV R3, #1			//Sorted flag

		Bubble_Loop:
			CMP R2, R1
			BGE Check_Flag

			LDRB R4, [R0, R2]
			ADD R2, R2, #1
			LDRB R5, [R0, R2]
			CMP R5, R4
			BGE Bubble_Loop

			STRB R4, [R0, R2]
			SUB R2, R2, #1
			STRB R5, [R0, R2]
			ADD R2, R2, #1
			MOV R3, #0
			B Bubble_Loop

		Check_Flag:
			CMP R3, #1
			IT EQ
				BXEQ LR
			SUB R1, R1, #1
			MOV R2, #0
			B Sort_Loop

//Blink LED on PC13 at 1Hz
Blink:
	// Create bit set mask
	MOV R4, #1
	LSL R4, #13
	// Create bit reset mask
	MOV R5, #1
	LSL R5, #29
	// Load the address of GPIOC_BSRR
	LDR R6, =GPIOC_BSRR
	Blink_Loop:
		STR R4, [R6]
		MOV R0, #500
		BL Delay
		STR R5, [R6]
		MOV R0, #500
		BL Delay
		B Blink_Loop

.section .data.main_assembly
.word SYST_CSR
SYST_CSR = 0xE000E010;
.word GPIOC_BSRR
GPIOC_BSRR = 0x40011010;
Arr:
	.byte 0xAA, 0xF1, 0x08, 0xCD, 0x78, 0x12, 0x34, 0xAD, 0x45, 0x92, 0x32, 0x57, 0xE6, 0x4E, 0x1C
